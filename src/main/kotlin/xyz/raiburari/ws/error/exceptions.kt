package xyz.raiburari.ws.error

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.notFound
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.context.request.WebRequest

class NotFoundException : Exception {
    constructor() : super()
    constructor(message: String?) : super(message)
    constructor(message: String?, cause: Throwable?) : super(message, cause)
    constructor(cause: Throwable?) : super(cause)
    constructor(message: String?, cause: Throwable?, enableSuppression: Boolean, writableStackTrace: Boolean) : super(message, cause, enableSuppression, writableStackTrace)
}

class EmailAlreadyTaken : Error {
    override fun toDto(): ErrorDto {
        return ErrorDto(
                status = HttpStatus.CONFLICT,
                message = this.message ?: ""
        )

    }

    companion object {
        val oMessage = "The username is already taken"
    }

    constructor() : super(oMessage)
    constructor(cause: Throwable?) : super(oMessage, cause)
    constructor(cause: Throwable?, enableSuppression: Boolean, writableStackTrace: Boolean) : super(oMessage, cause, enableSuppression, writableStackTrace)
}

@RestControllerAdvice
class Handler {
    @ExceptionHandler(value = [NotFoundException::class])
    fun notFoundHandler(ex: NotFoundException, rq: WebRequest): ResponseEntity<Any> {
        return notFound().build()
    }

    @ExceptionHandler(value = [EmailAlreadyTaken::class])
    fun emailAlreadyTakenHandler(ex: EmailAlreadyTaken, rq: WebRequest): ResponseEntity<Any> {
        val dto = ex.toDto()
        return ResponseEntity(dto, dto.status)
    }
}

