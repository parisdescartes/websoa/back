package xyz.raiburari.ws.error

import org.springframework.http.HttpStatus

data class ErrorDto(
        val status: HttpStatus,
        val message: String
)
