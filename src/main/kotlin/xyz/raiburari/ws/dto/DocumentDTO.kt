package xyz.raiburari.ws.dto

import xyz.raiburari.ws.data.entity.Document

data class DocumentDTO(
        val id: Long,
        val name: String

) {
    constructor(entity: Document) : this(entity.Id, entity.name)
}