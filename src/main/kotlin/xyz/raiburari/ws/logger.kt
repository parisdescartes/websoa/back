package xyz.raiburari.ws

import org.slf4j.Logger
import org.slf4j.LoggerFactory

inline fun <reified R : Any> logger(): Lazy<Logger> {
    return lazy { LoggerFactory.getLogger(R::class.java) }
}