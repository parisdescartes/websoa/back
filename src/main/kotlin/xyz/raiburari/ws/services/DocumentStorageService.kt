package xyz.raiburari.ws.services

import org.springframework.stereotype.Service
import org.springframework.util.StringUtils
import org.springframework.web.multipart.MultipartFile
import xyz.raiburari.ws.data.entity.Document
import xyz.raiburari.ws.data.entity.strToType
import xyz.raiburari.ws.users.entity.User
import xyz.raiburari.ws.users.repository.UserRepository

@Service
class DocumentStorageService(
        private val userRepository: UserRepository
) {
    fun store(user: User, file: MultipartFile) {
        val filename = StringUtils.cleanPath(file.originalFilename!!)
        if (filename.contains("..")) {
            throw IllegalArgumentException("Illegal pattern in filename")
        }
        val type = strToType(StringUtils.getFilenameExtension(filename)!!)

        user.documents.add(
                Document(name = filename, bytes = file.bytes, type = type))
        userRepository.save(user)
    }
}