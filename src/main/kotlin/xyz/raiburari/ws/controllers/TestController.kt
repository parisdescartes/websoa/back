package xyz.raiburari.ws.controllers

import org.springframework.http.ResponseEntity.ok
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class TestController {
    @GetMapping("")
    fun getslash() = ok("Hello World'")
}