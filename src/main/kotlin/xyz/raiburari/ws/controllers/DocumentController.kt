package xyz.raiburari.ws.controllers

import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import xyz.raiburari.ws.data.repositories.DocumentRepository
import xyz.raiburari.ws.dto.DocumentDTO
import xyz.raiburari.ws.error.NotFoundException
import xyz.raiburari.ws.services.DocumentStorageService
import xyz.raiburari.ws.users.security.UserDetailsImpl

@RestController
@RequestMapping("/documents")
class DocumentController(
        private val documentStorageService: DocumentStorageService,
        private var documents: DocumentRepository
        ) {

    @GetMapping("")
    fun all(@AuthenticationPrincipal principal: UserDetailsImpl): ResponseEntity<List<DocumentDTO>> {
        return ok(principal.user.documents.map { DocumentDTO(it) })
    }

    /**
     * Get a document per Id
     * @param id
     */
    @GetMapping("/{id}")
    fun one(@AuthenticationPrincipal principal: UserDetailsImpl, @PathVariable id: Long): ResponseEntity<DocumentDTO> {
        return ok(principal.user.documents
                .stream()
                .filter { it.Id == id }.findFirst().map { DocumentDTO(it) }
                .orElseThrow {
                    NotFoundException("Document with $id not found")
                })
    }

    @PostMapping("")
    fun createDocument(@AuthenticationPrincipal principal: UserDetailsImpl, @RequestParam file: MultipartFile) {
        documentStorageService.store(principal.user, file)
    }

    @GetMapping("/{id}/data", produces = [MediaType.APPLICATION_OCTET_STREAM_VALUE])
    fun getDocumentData(@PathVariable id: Long, @AuthenticationPrincipal principal: UserDetailsImpl): ResponseEntity<ByteArray> {
        return ok(principal.user.documents.stream()
                .filter { it.Id == id }.findFirst()
                .orElseThrow {
                    throw NotFoundException("Document with $id not found")
                }.bytes)
    }
}

