package xyz.raiburari.ws.data.repositories

import org.springframework.data.jpa.repository.JpaRepository
import xyz.raiburari.ws.data.entity.Document


interface DocumentRepository : JpaRepository<Document, Long>
