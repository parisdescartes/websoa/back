package xyz.raiburari.ws.data.entity

import javax.persistence.*

@Entity
data class Document(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var Id: Long = 0,
        var name: String = "",
        @Lob
        var bytes: ByteArray = ByteArray(0),
        var type: Type = Type.None
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Document) return false

        if (Id != other.Id) return false

        return true
    }

    override fun hashCode(): Int {
        return Id.hashCode()
    }
}
