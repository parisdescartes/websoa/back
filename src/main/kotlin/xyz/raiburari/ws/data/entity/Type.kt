package xyz.raiburari.ws.data.entity

fun strToType(str: String) = when (str) {
    "epub" -> Type.EPUB
    "pdf" -> Type.PDF
    else -> {
        throw IllegalArgumentException("Not compatible type")
    }
}


enum class Type {
    None,
    PDF,
    EPUB,
}