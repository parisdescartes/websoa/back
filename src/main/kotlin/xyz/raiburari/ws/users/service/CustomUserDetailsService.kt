package xyz.raiburari.ws.users.service

import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import xyz.raiburari.ws.users.repository.UserRepository
import xyz.raiburari.ws.users.security.UserDetailsImpl

@Service
class CustomUserDetailsService(private val userRepository: UserRepository) : UserDetailsService {

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        return UserDetailsImpl(
                userRepository.findOneByUsername(username) ?: throw UsernameNotFoundException(
                        "username:  '$username' not found"
                )
        )
    }
}