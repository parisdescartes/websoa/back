package xyz.raiburari.ws.users.security

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import xyz.raiburari.ws.logger
import xyz.raiburari.ws.users.entity.User
import java.util.stream.Collectors

class UserDetailsImpl(val user: User) : UserDetails {

    private val logger by logger<UserDetailsImpl>()

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return user.roles.stream()
                .map { role ->
                    logger.debug("Granting Authority to user with role: $role")
                    SimpleGrantedAuthority(role.toString())
                }
                .collect(Collectors.toList())
    }

    override fun isEnabled(): Boolean = user.enabled

    override fun getUsername(): String = user.username

    override fun isCredentialsNonExpired(): Boolean = user.credentialsNonExpired

    override fun getPassword(): String = user.password

    override fun isAccountNonExpired(): Boolean = user.accountNonExpired

    override fun isAccountNonLocked(): Boolean = user.accountNonLocked
}