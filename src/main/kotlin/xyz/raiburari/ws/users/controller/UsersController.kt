package xyz.raiburari.ws.users.controller

import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.*
import xyz.raiburari.ws.error.EmailAlreadyTaken
import xyz.raiburari.ws.error.NotFoundException
import xyz.raiburari.ws.logger
import xyz.raiburari.ws.users.entity.User
import xyz.raiburari.ws.users.repository.UserRepository
import xyz.raiburari.ws.users.security.UserDetailsImpl
import java.security.Principal


data class PostUser(val username: String, val password: String, val firstName: String, val lastName: String, val email: String)


@RestController
@RequestMapping("/users")
class UsersController(val userRepository: UserRepository, val encoderAndMatcherConfig: PasswordEncoder) {

    private val logger by logger<UsersController>()

    @PostMapping("")
    fun postUser(@RequestBody body: PostUser): ResponseEntity<User> {
        logger.info("Adding an user with the username: ${body.username}")

        if (userRepository.findOneByEmail(body.email) != null) {
            logger.error("User already exists : ${body.email}")
            throw EmailAlreadyTaken()
        }
        val userToAdd = User()

        userToAdd.firstname = body.firstName
        userToAdd.lastname = body.lastName
        userToAdd.email = body.email
        userToAdd.username = body.username
        userToAdd.password = encoderAndMatcherConfig.encode(body.password)
        userRepository.save(userToAdd)
        return ok(userToAdd)
    }

    @GetMapping("")
    fun all() = ok(userRepository.findAll())

    @GetMapping("/me")
    fun me(principal: Principal) = ok(principal)

    @PutMapping("/me")
    fun updateMe(@AuthenticationPrincipal userDetails: UserDetailsImpl, @RequestBody body: PostUser): ResponseEntity<Any> {
        val user = userRepository.findOneByUsername(userDetails.username)
                ?: throw NotFoundException()

        user.username = body.username
        user.password = body.password

        return ok(userDetails.user)
    }
}

