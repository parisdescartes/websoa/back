package xyz.raiburari.ws.users.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id


@Entity
open class Role(
        @Id
        @GeneratedValue
        open var id: Long = 0,
        open var rolename: String = ""
)
