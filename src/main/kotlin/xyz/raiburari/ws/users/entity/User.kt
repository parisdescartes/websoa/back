package xyz.raiburari.ws.users.entity

import xyz.raiburari.ws.data.entity.Document
import java.util.*
import javax.persistence.*

@Entity
open class User(
        open var firstname: String = "",
        open var lastname: String = "",
        open var username: String = "",
        open var email: String = "",
        open var password: String = "",

        @Id
        @GeneratedValue
        open var id: Long = 0,
        open var version: Int = 0,
        open var accountNonExpired: Boolean = true,
        open var accountNonLocked: Boolean = true,
        open var credentialsNonExpired: Boolean = true,
        open var enabled: Boolean = true,

        @OneToMany(fetch = FetchType.EAGER, cascade = [CascadeType.ALL], targetEntity = Role::class)
        open var roles: Set<Role> = HashSet(),

        @OneToMany(fetch = FetchType.EAGER, cascade = [CascadeType.ALL], targetEntity = Document::class)
        open var documents: MutableList<Document> = mutableListOf()


)
