package xyz.raiburari.ws.users.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import xyz.raiburari.ws.users.entity.Role

@Repository
interface RoleRepository : JpaRepository<Role, Long> {
    fun findByRolename(rolename: String): Role
}