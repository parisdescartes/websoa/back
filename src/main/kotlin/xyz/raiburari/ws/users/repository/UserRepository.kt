package xyz.raiburari.ws.users.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import xyz.raiburari.ws.users.entity.User

@Repository
interface UserRepository : JpaRepository<User, Long> {
    fun findOneByUsername(userName: String): User?
    fun findOneByEmail(email: String): User?
}