FROM gradle:latest as builder
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build

FROM openjdk:11-jre-slim
EXPOSE 8080
COPY --from=builder /home/gradle/src/build/distributions/raiburari-boot.tar /app/
WORKDIR /app
RUN tar -xvf raiburari-boot.tar
WORKDIR /app/raiburari-boot
CMD bin/raiburari-boot
